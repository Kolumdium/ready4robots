# -*- coding: utf-8 -*-
"""
Created on Mon Mar  8 15:27:52 2021

@author: plank
"""

import pandas as pd
from pathlib import Path
from matplotlib.lines import Line2D
from matplotlib import rcParams, cycler
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import math

import osmnx as ox
import argparse
from configobj import ConfigObj
import configreader as cfr
import ready4robots as osmq
import osmhelper as oh
import networkx as nx
import matplotlib.lines as mlines
import matplotlib.ticker as mtick

import configreader as cfr
from mpl_toolkits.axes_grid1 import make_axes_locatable

def calc_normalized_vectors2(hists_f, hists_d, szn, nodes, max_nodes, scale_x = False):
    """
        nodes is the amount of nodes in the filtered graph of the scenario
        max_nodes amount of nodes in the unfiltered graph

        avg reachable nodes is the ammount nodes that are reached on average
    """

    avgs = []
    unreachables = []
    l_x = []
    l_y = []
    results = []
    reachable_ways = []
    reachable_nodes_avg = []
    percentage_ways = []
    percentage_nodes = []

    counter = 0
    for hist_d in hists_d:
        df = pd.DataFrame(hist_d, index=[0])
        
        scenario_max_reachable_nodes = max_nodes[counter]
        max_ways = max_nodes[counter] ** 2
        unreachable_ways = df["inf"].iloc[0]
        
        print(scenario_max_reachable_nodes, max_nodes[counter], nodes[counter])
        # detours are only counted if the path is longet than 0

        df2 = df.drop(["inf", "self"], axis=1, errors="ignore")
        scenario_reachable_ways = df2.T.sum().iloc[0]
        scenario_avg_reachable_nodes = scenario_reachable_ways / scenario_max_reachable_nodes
        
        percentage_scenario_nodes = scenario_avg_reachable_nodes / scenario_max_reachable_nodes
        # print(scenario_ways, scenario_ways_max, percentage_scenario_ways)
        print("Check check",scenario_avg_reachable_nodes, scenario_max_reachable_nodes, percentage_scenario_nodes)
        avg = avg_detour_percentage(df.drop("self", axis = 1, errors="ignore"))
        avgs.append(avg)
        unreachable_nodes = 1 - scenario_avg_reachable_nodes
        unreachables.append(unreachable_nodes)
        # reachable_ways.append(scenario_ways)
        reachable_nodes_avg.append(scenario_avg_reachable_nodes)
        percentage_nodes.append(percentage_scenario_nodes)
        # percentage_ways.append(percentage_scenario_ways)
        counter += 1

    reachable_min = 0
    reachable_max = max_nodes[0]
    #input()
    avg_min = min(avgs)
    avg_max = max(avgs)
    
    
    for avg_c, reachable_c in zip(avgs, nodes):
        #unreachable_c = max_nodes - reachable_c
        print(avg_c, reachable_c)
        x = (avg_c - avg_min) / (avg_max - avg_min)
        y = 1 - (reachable_c / reachable_max)
        # 100 - 10 / 100 - 0
        vector = math.sqrt(x ** 2 + y ** 2)
        results.append(vector)
        if scale_x:
            print(x , y, vector)
            l_x.append(x)
        else:
            print(avg_c , y, vector)
            l_x.append(avg_c)
        l_y.append(y)
    
    dfr = pd.DataFrame(szn)
    dfr["vector"] = results
    dfr["detours"] = l_x
    dfr["unreachables"] = l_y
    dfr["avg_reachables"] = percentage_nodes

    print(dfr)
    
    return dfr

def city_plots(cfg, city, override = False):
    # //Histogram of Pathlength Distribution per Scenario
    
    # Scatterplot of Pathlength Distribution for all Scenarios
    # scatter_pathlength_city(city, cfg)
    
    # Quiver of the vector (avg, unreachable) per scenario
    # quiver_of_city(city, cfg, scale_x = True)
    # quiver_of_city(city, cfg, scale_x = False)
    #quiver_as_points(city, cfg, type="tot", scale_x = False, override = override)
    #quiver_as_points(city, cfg, type="avg", scale_x = False, override = override)
    
    # Histogram of pop reachable from nodes per traveltime per scenario
    # assumptions:
        # the maximum reachable population should change.
        # the poulation should shift more to the left for faster scenarios
    # hist_pop_city(city, cfg)
    # Diagram with t1 - tn with possible estm. function
        # as well as T which is t until pop = 95% is reached.
        # right axis total, left axis max of total pop in city (not just the scenario but in the "cut")
    #line_pop_city(cfg, city, folder = Path("out"))
    
    # #color the scenarios in the city to see which can go where.
    color_city(city, cfg)
    
    
    #pop density
    # pop_density(city,cfg)
    
    # plot the nodes with the different reachable popvalues and their "normal" values and maybe also a x/y
    # reachable_pop(city, cfg)

def cities_plots(cfg,scale_x = False, override = False):

    #plot_cities_compare(cfg, scale_x = scale_x)
    cities_quiver_as_points(cfg, type="tot", scale_x = scale_x, override=override)
    cities_quiver_as_points(cfg, type="avg", scale_x = scale_x, override=override)

def len_of_vector(directions_x, directions_y):

    z = []
    for x, y in zip(directions_x, directions_y):
        z.append(math.sqrt(x ** 2 + y ** 2))


def get_cities_results_as_df(cfg, type = "tot", scale_x = False, override = False):

    #TODO get from cfg
    scenarios = cfr.get_scenario_names(cfg)
    num_sz = len(scenarios)
    
    colors = cfr.get_colors(cfg)
    markers = cfr.get_markers(cfg)

    cities_tot = []
    detours_tot = []
    unreachables_tot = []
    unreachables_avg = []

    length_tot = []
    colors_tot = []
    markers_tot = []
    
    counter = 0
    
    cities = cfr.get_cities(cfg)

    for city in cities:
        print(city)
        detours, unreachables, lengths, avg_reachables = quiver_of_city(city, cfg, scale_x = scale_x,
                                                        return_data=True, override=override)
        cities_tot.extend([city for x in range(0,num_sz)])
        detours_tot.extend(list(detours))
        unreachables_tot.extend(list(unreachables))
        unreachables_avg.extend(list(avg_reachables))
        length_tot.extend(list(lengths))
        colors_tot.extend(colors)
        markers_tot.extend([markers[counter] for x in range(0,num_sz)])
        counter += 1

    print("DEBUG")
    print(detours)
    print([city for x in range(0,num_sz)])

    df = pd.DataFrame()
    df["cities"] = cities_tot
    df["lengths"] = length_tot
    df["detours"] = detours_tot
    df["color"] = colors_tot
    df["markers"] = markers_tot

    if type == "tot":
        df["unreachables"] = unreachables_tot
    elif type == "avg":
        df["unreachables"] = unreachables_avg
    else:
        raise ValueError

    return df

def plot_cities_unreachables(df,scale_x):

    ax = df.plot.scatter(x = "cities", y = "unreachables", s = 100, c = "color",
                         marker = "+", figsize=(8,8))
    ax.set_xlabel("Cities")
    ax.set_ylabel("Percentage of unreachable nodes")
    ax.set_title("Comparision of different cities")
    # ax.xticks(fontsize=20)
    for tick in ax.get_xticklabels():
        tick.set_rotation(90)
        tick.set_fontsize(15)

    custom_lines = make_custom_legend(cfr.get_colors(cfg), cfr.get_scenario_names(cfg))

    ax.legend(handles=custom_lines, loc="best")
    
    fig = ax.get_figure()
    fig.savefig(Path("out", "cities_results_unrechables_{}.pdf".format(scale_x)), bbox_inches='tight')

def plot_cities_detours(df,scale_x):

    ax = df.plot.scatter(x = "cities", y = "detours", s = 100, c = "color",
                         marker = "+", figsize=(8,8))
    ax.set_xlabel("Cities")
    ax.set_ylabel("detours normalised")
    ax.set_title("Comparision of different cities")
    for tick in ax.get_xticklabels():
        tick.set_rotation(90)
        tick.set_fontsize(15)

    custom_lines = make_custom_legend(cfr.get_colors(cfg), cfr.get_scenario_names(cfg))

    ax.legend(handles=custom_lines, loc="center")
    fig = ax.get_figure()
    fig.savefig(Path("out", "cities_results_detours_{}.pdf".format(scale_x)), bbox_inches='tight')

def plot_cities_vectors(df,scale_x):

    ax = df.plot.scatter(x = "cities", y = "lengths", s = 100, c = "color",
                         marker = "+", figsize=(8,8))
    ax.set_xlabel("Cities")
    ax.set_ylabel("Vectorlength")
    ax.set_title("Comparision of different cities")
    
    for tick in ax.get_xticklabels():
        tick.set_rotation(90)
        tick.set_fontsize(15)

    custom_lines = make_custom_legend(cfr.get_colors(cfg), cfr.get_scenario_names(cfg))

    ax.legend(handles=custom_lines, loc="center")
    fig = ax.get_figure()
    fig.savefig(Path("out", "cities_results_vector_{}.pdf".format(scale_x)), bbox_inches='tight')

def plot_cities_compare(cfg, scale_x = False, override = False):
    
    scenarios = cfr.get_scenario_names(cfg)
    colors = cfr.get_colors(cfg)

    df = pd.DataFrame(index = scenarios)

    ax = None

    df = get_cities_results_as_df(cfg, scale_x, override = override)
    df = df.sort_values( by = ["color", "lengths"])
    
    print(df)
    plot_cities_detours(df,scale_x)
    plot_cities_unreachables(df,scale_x)
    plot_cities_vectors(df,scale_x)

    
 
def color_city(city, cfg):
    
    fm = None
    for sz in cfr.yield_scenarios(cfg):
        if sz["name"] == "Optimal":
            continue
        if sz["name"] == "Small":
            width = 2
        else:
            width = 1
        speeds = cfr.get_speeds(cfg, sz["name"])
        
        scenario_filter = osmq.get_scenario_filter(cfg, sz["name"], speeds = speeds)
            
        G_nx = osmq.get_graph(city)
        H_nx = oh.filter_graph_by_dict(G_nx, scenario_filter)
        
        
        fm = ox.plot_graph_folium(
            H_nx, graph_map=fm, popup_attribute="highway",
            edge_color = get_scenario_color(cfg, sz["name"]),edge_width = width)
        
        fm2 = None
        fm2 = ox.plot_graph_folium(
            H_nx, graph_map=fm2, popup_attribute="highway",
            edge_color = get_scenario_color(cfg, sz["name"]),edge_width = width)
        
        fm2.save(str(Path("out", city + "_" + sz["name"] + ".html")))

    fm.save(str(Path("out", city + "_" + "all" + ".html")))
        
        #irgendwie einfärben
    pass
 
def dicts_to_df(dicts):
    
    df = pd.DataFrame()
    for d in dicts:
        df2 = pd.DataFrame.from_dict(d)
        df = df.append(df2)
    
    return df

def get_avg_and_unreachables(hist_dict):
    
    avg = 0
    inf = 0
    df = pd.DataFrame(hist_dict, index=[0])
    inf = df["inf"].iloc[0]
    df = df.drop([0, "inf"], axis = 1, errors = "ignore")
    df = df.T.reset_index().rename(columns={"index": "length", 0: "ammount"})
    df["multi"] = df["length"] * df["ammount"]
    sums = df.sum()
    avg = round(sums["multi"] / sums["ammount"], 4)
    
    return avg, inf

def get_scenario_color(cfg, scenario_name):
    colors = cfr.get_colors(cfg)
    scenarios = cfr.get_scenario_names(cfg)

    return colors[scenarios.index(scenario_name)]

def hist_pop_city(city, cfg, folder = None):
    
    lengths = [60, 300, 600, 1200, 1800, 3600]
    
    for sz in cfr.yield_scenarios(cfg):
        hist, pop = osmq.analyse_scenario(city, cfg, sz, folder)
        df = pd.DataFrame(pop)
        df.index = lengths
        df = df.T.reset_index(drop = True)
        hist_pop_scenario(df, city, sz["name"] )

def hist_pop_scenario(df, city, scenario_name ):
    figsize = (12,12)
    ax = df.plot.hist(alpha = 0.5, grid = True, bins=50, figsize = figsize)
    ax.set_ylabel("Frequency")
    ax.set_xlabel("Population reachable in timeframe")
    title = "Histogram der Population von {}".format("test")
    ax.set_title(title)
    fig = ax.get_figure()
    fig.savefig(str(Path("out", city + "_pop_{}".format(scenario_name))))
    plt.close(fig)

def line_pop_city(cfg, city, folder = None):
    lengths = [60, 300, 600, 1200, 1800, 3600, 360000]
    pop_total = osmq.get_max_pop(cfg, city, "total")
    ax = None
    ax2 = None
    ax3 = None
    
    print(folder)
    key_numbers = pd.DataFrame(osmq.key_numbers_city(cfg, city))
    for sz in cfr.yield_scenarios(cfg):
        key_numbers_scenario = key_numbers[key_numbers["scenario_name"] == sz["name"]]
        print(key_numbers_scenario)
        _, _, _, pop_n = osmq.load_histograms(cfg, city, sz, folder=folder)
        df = pd.DataFrame(pop_n)
        df.index = lengths
        df = df.T.reset_index(drop = True)
        length = len(df.index)
        maxs = df.max()
        scenario_max_avg = maxs.iloc[-1]
        scenario_max_avg_percent = round((scenario_max_avg/pop_total )*100, 4)
        print(maxs)
        dfs = df.sum() / length
        # print(dfs)
        scenario_max = key_numbers_scenario["scenario_max"].iloc[0]
        scenario_max_percent = round((scenario_max/pop_total)*100, 4)
        print("The maximum for this scenario is {} which is {}%".format(
              scenario_max,scenario_max_percent))
        a = round((dfs / scenario_max) * 100, 4)
        df2 = pd.DataFrame(a)
        df2["sums"] = dfs
        df2["percent"] = a
        df2["percent_total"] = round((dfs / pop_total) * 100, 4)
        print(df2)
        
        # If the last row is the 100h time cut it out.
        
        last_row = df2.iloc[-1]
        print(last_row)
        if last_row.name == 360000:
            df2 = df2[:-1]
        else:
            print(last_row.index[0])
            last_row = None
        #The percentage of the theoretically reachable population
        #Node are filtered by degree. 
        #If they have atleast degree 1 they count to the maximum
        # ax = df2.plot.line(
        #     y = "percent", ax = ax, label = sz["name"],
        #     color = get_szeanrio_color(sz["name"]))
        
        ax = df2.plot.line( 
            y = "sums",
            ax = ax,
            label = sz["name"],
            color = get_scenario_color(cfg, sz["name"]))
        
        # ax2.set_ylimit()
        # secondary_axis = ax2.twinx()
        # secondary_axis.set_ylim([0, 100])
        ax.axhline(y = scenario_max,
                   xmin = 0.95,
                   xmax = 1,
                   color = get_scenario_color(cfg, sz["name"]))
        ax.axhline(y = scenario_max_avg,
                   xmin = 0.95,
                   xmax = 1,
                   color = get_scenario_color(cfg, sz["name"]),
                   linestyle = "dashed")

        
        if not last_row is None:
            print("Doing the Limit Line")
            ax.axhline(y = last_row["sums"],
                        xmin = 0,
                        xmax = 1,
                        color = get_scenario_color(cfg, sz["name"]),
                        linestyle="dotted")
        else:
            print(last_row)
        
        ax2 = df2.plot.line( 
            y = "percent_total",
            ax = ax,
            label = sz["name"],
            color = get_scenario_color(cfg, sz["name"]),
            secondary_y = True,
            legend = False)
        
        ax2.axhline(y = scenario_max_percent,
                    xmin = 0.0,
                    xmax = 0.05,
                    color = get_scenario_color(cfg, sz["name"]))
        ax2.axhline(y = scenario_max_avg_percent,
                    xmin = 0.0,
                    xmax = 0.05,
                    color = get_scenario_color(cfg, sz["name"]),
                    linestyle = "dashed")

        ax.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
            ncol=3, mode="expand", borderaxespad=0.)
        
        ax.set_title("Population Accessibility of {}".format(city), pad=60)
        ax.set_xlabel("Time in Seconds")
        ax.set_ylabel("Population Reachable")
        ax2.set_ylabel("Population in Percent")
        
        
        
        # ax3 = df2.plot.line( 
        #     y = "percent_total", ax = ax3, label = sz["name"],
        #     color = get_szeanrio_color(sz["name"]))
        
        
        # ax3.axhline(y = scenario_max_percent, xmin = 0.95, xmax = 1, color = get_szeanrio_color(sz["name"]))

        # l = Line2D(
        #     [1,10], [2,20],
        #     linestyle = "--")
        # ax.lines.append(l)
        
        # l2 = Line2D([3000,3600], [scenario_max,scenario_max])
        # ax2.lines.append(l2)
        
        # l3 = Line2D([3000,3600], [scenario_max_percent,scenario_max_percent])
        # ax3.lines.append(l3)
        
    ax.grid(True, "major", "both")
    fig = ax.get_figure()
    fig.savefig(Path("out", "population-line-{}.pdf".format(city)),
                bbox_inches='tight')  
        


def places_avg_path_plot(df, citynumber):
    
    c = []
    
    for row in df.itertuples():
        if row.Index == "small":
            color = "red"
        elif row.Index == "medium":
            color = "green"
        elif row.Index == "large":
            color = "black"
        elif row.Index == "magic":
            color = "orange"
        c.append(color)
    
    ax1 = df.plot.scatter(x ="city", y = "vector", s = 100, marker = "1",
                          c=c,figsize=(8,12))
    for tick in ax1.get_xticklabels():
        tick.set_rotation(90)
    
    custom_lines = make_custom_legend(cfr.get_colors(cfg), cfr.get_scenario_names(cfg))

    custom_lines.extend(
        [Line2D([], [], color='black', marker='1', linestyle='None',
                          markersize=10, label='avg Traveltime'),
        Line2D([], [], color='black', marker='+', linestyle='None',
                          markersize=10, label='unreachable Nodes')])

    ax1.legend(handles=custom_lines, loc="center")

    fig = ax1.get_figure()
    fig.savefig(Path("out", "together", "first_try"))    


def quiver_as_points(city, cfg, type = "tot", scale_x = False, return_data = False, override = False):
    

    detours, unreachables, _, avg_r = quiver_of_city(city, cfg, scale_x, return_data=True, override = override)

    df = pd.DataFrame()
    if type == "tot":
        df["unreachables"] = unreachables 
    elif type == "avg":
        df["unreachables"] = avg_r
    else:
        raise ValueError("Quiver type not understood.")
    df["detours"] = detours

    
    colors = cfr.get_colors(cfg)
    print(df)
    counter = 0
    for row in df.itertuples():
        print(row)
        plt.scatter(x = row.detours, y = (row.unreachables * 100), c = colors[counter], marker = "X", s = 200)
        counter += 1
    # fig = ax.get_figure()
    # TODO Beschriftung und Legende
    ax = plt.gca()
    if type == "tot":
        ax.set_xlabel(label=u"\u0394ti", fontsize = 15)
        ax.set_ylabel(label="$n_i$", fontsize = 15)
        ax.set_title("Time and total node count comparison", fontsize = 15)
    elif type == "avg":
        ax.set_xlabel("Percentage of time spent more than the optimal", fontsize = 15)
        ax.set_ylabel("Percentage of nodes that can be reached on average", fontsize = 15)
        ax.set_title("Time and average reachable node comparison", fontsize = 15)

    custom_lines = make_custom_legend(cfr.get_colors(cfg), cfr.get_scenario_names(cfg))

    ax.legend(handles=custom_lines, loc="best")
    fig = ax.get_figure()
    fig.savefig(Path("out", "{}_quiver_as_points_{}_{}.pdf".format(city, type, scale_x)))
    plt.close()


def cities_quiver_as_points(cfg, type="tot", scale_x = False, override = False):

    df = get_cities_results_as_df(cfg, type, scale_x, override=override)
    print(df)
    ax = None
    city_marker = {}
    S1 = 0
    M1 = 0 
    M2 = 0
    L1 = 0
    O1 = 0
    for row in df.itertuples():
        
        plt.scatter(x = row.detours, y = row.unreachables * 100, c = row.color, marker = row.markers)
        city_marker[row.cities] = row.markers
    # fig = ax.get_figure()
    #print(df.describe())
    # TODO Beschriftung und Legende
    #input()
    ax = plt.gca()
    ax.set_axisbelow(True)
    ax.grid(True, "major", "both")
    ax.tick_params(labelsize = 14)

    if type == "tot":
        ax.set_xlabel(xlabel=u"$\Delta T_i$", fontsize = 15)
        ax.set_ylabel(ylabel="$c_i$", fontsize = 15)
        ax.set_title("Time and total node count comparison", fontsize = 15)
    elif type == "avg":
        ax.set_xlabel(xlabel=u"$\Delta T_i$", fontsize = 15)
        ax.set_ylabel(ylabel="$u_i$", fontsize = 15)
        ax.set_title("Time and average reachable node comparison", fontsize = 15)

    custom_lines = make_custom_legend(cfr.get_colors(cfg), cfr.get_scenario_names(cfg))

    custom_markers = []
    for key, value in city_marker.items():
        print(key, value)
        custom_markers.append(Line2D([0], [0], color = "black", linestyle='', markerfacecolor='black', markersize=10, label = key, marker = value))
    
    custom_lines.extend(custom_markers)

    ax.legend(handles=custom_lines, bbox_to_anchor=(1,1), loc="upper left")
    fig = ax.get_figure()
    fig.savefig(Path("out", "citites_quiver_as_points_{}_{}.pdf".format(type, scale_x)), bbox_inches='tight')
    plt.close()

def make_custom_legend(colors, names, markers = None):

    custom_lines = []
    if markers is None:
        markers = [1 for x in range(0, len(names))]
    for color, name, marker in zip(colors, names, markers):
        custom_lines.append(Line2D([0], [0], color=color, lw=4, label=name, marker = marker))

    return custom_lines

def quiver_of_city(city, cfg, scale_x = False, return_data = False, override = False):
    
    hists_f = []
    hists_d = []
    szn = []

    colors = cfr.get_colors(cfg)
    
    df = pd.DataFrame()
    
    path_quiver = Path("out", "quiver_{}_{}_save.csv".format(city, scale_x))
    max_nodes = []
    nodes = []
    if path_quiver.is_file() and not override:
        df = pd.read_csv(path_quiver)
    else:
        for sz in cfr.yield_scenarios(cfg):
            
            if sz["name"] == "Optimal":
                hists_f.append(hist_n)
                no_detour = {}
                sum = 0
                for key, value in hist_n.items():
                    if key == "inf":
                        no_detour[key] = value 
                        continue
                    elif key == "self":
                        no_detour[key] = value
                        continue

                    sum += value
                no_detour[100] = sum
                hists_d.append(no_detour)
            else:
                hist_n, hist_f, hist_d, pop_n = osmq.analyse_scenario(city, cfg, sz)
                hists_f.append(hist_f)
                hists_d.append(hist_d)
                # print(hist_d)
                # input()
            nodes.append(hist_n["self"])
            max_nodes.append(hist_f["self"])
            szn.append(sz["name"])
    
        print(nodes, max_nodes)
        df = calc_normalized_vectors2(hists_f, hists_d, szn, nodes, max_nodes, scale_x=scale_x)
        
        df.to_csv(path_quiver)
    
    zero = [0,0,0,0]
    x_directions = df["detours"]
    y_directions = df["unreachables"]
    avg_reachables = df["avg_reachables"]
    lengths = df["vector"]
    
    

    if return_data:
        return x_directions, y_directions, lengths, avg_reachables

    angle = np.linspace( 0 , 2 * np.pi , 150 ) 
 
    radius = 1
    radius_lower = df["vector"].min()
    
    x = radius * np.cos( angle ) 
    y = radius * np.sin( angle ) 
     
    figure, ax = plt.subplots( 1 ) 
     
    if scale_x:
        ax.plot( x, y, color = "black" ) 
    else:
        ax.plot( x, y, color = "white" ) 

    u = radius_lower * np.cos( angle ) 
    v = radius_lower * np.sin( angle ) 
    
    ax.plot( u, v, color = "black" ) 

    ax.set_aspect( 1 ) 
    
    ax.set_xlabel("Average detour path length")
    ax.set_ylabel("Unreachable ways")
    # print(szn)
    # print(color)
    
    custom_lines = make_custom_legend(cfr.get_colors(cfg), cfr.get_scenario_names(cfg))
    
    ax.legend(handles=custom_lines, loc="best")
    
    
    # ax.legend(szn, ["red", "green", "black", "orange"])
    q = plt.quiver(zero, zero, x_directions, y_directions, angles='xy', scale_units='xy', scale=1, color = color)

    if scale_x:
        plt.xlim(-0.03, 1.2)
    else:
        plt.xlim(-0.03, 10)
        

    plt.ylim(-0.03, 1.2)
    plt.title(city)
    fig = ax.get_figure()
    fig.savefig(Path("out", city + "_quiver_{}.pdf".format(scale_x)))
    fig.close()
    plt.close()

def pop_density(city, cfg):
    for sz in cfr.yield_scenarios(cfg):
        speeds = cfr.get_speeds(cfg, sz["name"])
        
        scenario_filter = osmq.get_scenario_filter(cfg, sz["name"], speeds = speeds)
            
        G_nx = osmq.get_graph(city, got_pop_data=True)
        
        nc = ox.plot.get_node_colors_by_attr(G_nx, "pop", cmap="Greens")

        fig, ax = ox.plot_graph(
            G_nx,
            node_color=nc,
            node_size=3,
            node_zorder=2,
            edge_linewidth=0.1,
            edge_color="k",
            bgcolor="w",
        )
        fig.savefig('out\pop_density_{}_{}.svg'.format(city, sz["name"]))
  
def pop_density_hexbin(city):
    df = pd.read_csv("csvs/{}-10m.csv".format(city))
    df.columns = ["i", "lon", "lat", "pop"]
    print(df.columns)
    
    #workaround for showing x labels...
    #https://stackoverflow.com/questions/43121584/matplotlib-scatterplot-x-axis-labels
    fig, ax = plt.subplots()

    ax = df.plot.hexbin(
        x='lon', y='lat', C="pop",
        xlabel = "lon", ylabel = "lat",
        ax = ax,
        reduce_C_function=np.sum, gridsize=25, title = "Freiberg"
        )
  
def reachable_pop(city, cfg):
    
    cmap = plt.cm.get_cmap('Greens')
     
    # plt.rc('text', usetex=True)
    # plt.rc('font', family='serif')
    
    for sz in cfr.yield_scenarios(cfg):
        print(sz)
        _,_,_, pop = osmq.load_histograms(cfg, city, sz)
        G_nx = osmq.get_graph(city, got_pop_data=False)
        reachable_pop = {}
        
        for szc in range(0,6):
            for node in G_nx.nodes():
                reachable = pop.get(node, [0,0,0,0,0,0])
                reachable_pop[node] = reachable[szc]
            
            nx.set_node_attributes(G_nx, reachable_pop, "pop")
            nodes = ox.graph_to_gdfs(G_nx, nodes = True, edges = False)
            
            # print()
            # nodes = nodes[nodes[pop] > 0.1]
            
            # norm = plt.Normalize(vmin=0, vmax=40657)
            norm=plt.Normalize(vmin=nodes['pop'].min(), vmax=nodes['pop'].max())
            sm = mpl.cm.ScalarMappable(norm=norm, cmap=cmap)
            sm.set_array([])
            
            speeds = cfr.get_speeds(cfg, sz["name"])
            scenario_filter = osmq.get_scenario_filter(cfg, sz["name"], speeds = speeds)
            H_nx = oh.filter_graph_by_dict(G_nx, scenario_filter)
            
            #Just a hack
            # first = list(H_nx)[0]
            # H_nx.nodes[first]["pop"] = 40657
            
            nc = ox.plot.get_node_colors_by_attr(H_nx, "pop", cmap="Greens")
            # for x in nc:
            #     print(x)
            #     break
            na = [0 if x < 100 else 1 for x in nodes['pop']]
            # print(nc)
            # print(na)
            fig, ax = ox.plot_graph(
                H_nx,
                node_color=nc,
                node_alpha=na,
                node_size=10,
                node_zorder=0,
                edge_linewidth=0.5,
                edge_color="black",
                bgcolor="white",
                figsize = (12,12),
                dpi=600,
                show=False,
            )
            # ax.set_frame_on(False)
            # ax.axis('off')
            
            cax = fig.add_axes([ax.get_position().x0,
                                ax.get_position().y0-0.04,
                                ax.get_position().width,
                                0.03])
            
            cb = fig.colorbar(mpl.cm.ScalarMappable(norm=norm, cmap=cmap),
                              cax=cax,
                              orientation='horizontal')
            
            cb.set_label('Population reachable', fontsize = 15)
            #fig.savefig('out\{}_{}_{}.png'.format(city, sz["name"], szc))
            fig.savefig('out\{}-{}-{}s.pdf'.format(city, sz["name"], cfr.get_traveltimes(cfg)[szc]), bbox_inches="tight", pad_inches = 0)
            
            
            
        
        
    szc += 1

def scatter_pathlength_city(city, cfg):
    
    ax = None
    figsize = (12,12)
    for sz in cfr.yield_scenarios(cfg):
        hist, pop = osmq.analyse_scenario(city, cfg, sz)
        df = pd.DataFrame(hist, index = [0]).drop(columns=['inf', 0] ).T.reset_index()
        df.rename(columns={"index":"travel time in s", 0: "occurence"}, inplace=True)
        color = get_scenario_color(cfg, sz["name"])
        ax = df.plot.scatter(x = "travel time in s", y = "occurence",
                           figsize = figsize, ax = ax, color = color,
                           label = sz["name"])
        
    fig = ax.get_figure()
    fig.savefig(Path("out", city + "_pathlength"))
    
def folium_test(city, cfg):
    
    for sz in cfr.yield_scenarios(cfg):
        speeds = cfr.get_speeds(cfg, sz["name"])
        
        scenario_filter = osmq.get_scenario_filter(cfg, sz["name"], speeds = speeds)
            
        G_nx = osmq.get_graph(city)
        H_nx = oh.filter_graph_by_dict(G_nx, scenario_filter)
        
        fm = ox.plot_graph_folium(H_nx, popup_attribute="highway")
        fm.save(str(Path("out", city + "_" + sz["name"] + ".html")))

def plot_components(cfg, zoomed = False, normalize = True):

    cities = cfr.get_cities(cfg)
    colors = cfr.get_colors(cfg)
    scenarios = cfr.get_scenario_names(cfg)
    markers = cfr.get_markers(cfg)

    replaces_color = dict(zip(scenarios, colors))
    replaces_markers = dict(zip(cities, markers))

    df = pd.DataFrame()
    for city in cities:
        df = pd.concat([df, pd.read_csv(Path("out", "{}_components.csv".format(city)))])
    
    ##load each city, count nodes, divide the components by the max_nodes.

    df.columns = ("counter", "cities", "scenario", "biggest_component_coverage", "component_count")

    print(df)


    df[["color"]] = df[["scenario"]].replace(replaces_color)
    df[["marker"]] = df[["cities"]].replace(replaces_markers)

    df2 = pd.read_csv(Path("out", "node_count.csv"))
    print(df2)
    node_counts = []
    for row in df.itertuples():
        # print(row)
        for row2 in df2.itertuples():
            if row.cities == row2.cities:
                node_counts.append(row2.node_count)

    #df = df.replace(replaces_markers)
    df["node_count"] = node_counts
    print(df)
    #per scenario
    df["component_count_norm"] = df["component_count"] / df["node_count"]
    #per max
    # df["component_count_norm"] = df["component_count"] / df["node_count"].max()
    # can#t set markers as a list so iterate instead
    # ax = df.plot.scatter(x = "2", y = "3", c = "1", marker = markers)

    #df = df.rename(columns={"0": "counter",  1: "marker", 2:"color", 3:"biggest_percentage_component", 4:"component_count"})
    print(df)
    city_marker = {}
    for row in df.itertuples():
        #print(row)
        # zoomed, medium, optimal 
        #      0,      *,       *  --> 1
        #      1,      1,       1, --> 1 
        #      1,      0,       0, --> 0


        if normalize:
            if not zoomed:
                plt.scatter(x = row.biggest_component_coverage, y = row.component_count_norm * 100, c = row.color, marker = row.marker)
                city_marker[row.cities] = row.marker
            elif (row.scenario == "Medium" or row.scenario == "Optimal"):
                plt.scatter(x = row.biggest_component_coverage, y = row.component_count_norm * 100, c = row.color, marker = row.marker)
                city_marker[row.cities] = row.marker
        else:
            if not zoomed:
                plt.scatter(x = row.biggest_component_coverage, y = row.component_count * 100, c = row.color, marker = row.marker)
                city_marker[row.cities] = row.marker
            elif (row.scenario == "Medium" or row.scenario == "Optimal"):
                plt.scatter(x = row.biggest_component_coverage, y = row.component_count * 100, c = row.color, marker = row.marker)
                city_marker[row.cities] = row.marker

    # fig = ax.get_figure()
    # TODO Beschriftung und Legende

    ax = plt.gca()
    ax.set_axisbelow(True)
    ax.grid(True, "major", "both")
    ax.tick_params(labelsize = 14)

    custom_lines = make_custom_legend(colors, scenarios)

    custom_markers = []
    for key, value in city_marker.items():
        print(key, value)
        custom_markers.append(Line2D([0], [0], color = "black", linestyle='', markerfacecolor='black', markersize=10, label = key, marker = value))

    custom_lines.extend(custom_markers)

    ax.legend(handles=custom_lines, bbox_to_anchor=(1,1), loc="upper left")   
    ax.set_xlabel("Coverage of the largest component in %", fontsize = 15)
    ax.set_ylabel("Component count", fontsize = 15)
    ax.set_title("Component analyses", fontsize = 15)
    # ax.ticklabel_format(style='sci', axis='y')
    # ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.0e'))
    if not normalize:
        formatter = mtick.ScalarFormatter(useMathText=True)
        formatter.set_scientific(True) 
        formatter.set_powerlimits((-1,1)) 
        ax.yaxis.set_major_formatter(formatter) 
    fig = ax.get_figure()
    
    fig.savefig(Path("out", "cities_components_zoomed_{}_norm_{}.pdf".format(zoomed, normalize)), bbox_inches='tight')


    plt.close()

def save_fig(colors, scenarios, city_marker, sci = False, x = "", y = ""):
    ax = plt.gca()
    ax.set_axisbelow(True)
    ax.grid(True, "major", "both")
    ax.tick_params(labelsize = 14)

    custom_lines = make_custom_legend(colors, scenarios)

    custom_markers = []
    for key, value in city_marker.items():
        print(key, value)
        custom_markers.append(Line2D([0], [0], color = "black", linestyle='', markerfacecolor='black', markersize=10, label = key, marker = value))

    custom_lines.extend(custom_markers)

    ax.legend(handles=custom_lines, bbox_to_anchor=(1,1), loc="upper left")   
    ax.set_xlabel("{}".format(x), fontsize = 15)
    ax.set_ylabel("{}".format(y), fontsize = 15)
    ax.set_title("{} over {}".format(x, y), fontsize = 15)
    if sci:
        ax.ticklabel_format(style='sci', axis='y')
        ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.0e'))
        formatter = mtick.ScalarFormatter(useMathText=True)
        formatter.set_scientific(True) 
        formatter.set_powerlimits((-1,1)) 
        ax.yaxis.set_major_formatter(formatter) 
    fig = ax.get_figure()
    
    fig.savefig(Path("out", "{}_over_{}.pdf".format(x,y)), bbox_inches='tight')

def plot_x_y(df, param1, param2):
    city_marker = {}

    for row in df.itertuples():
        plt.scatter(x = getattr(row, param1), y = getattr(row, param2), c = row.color, marker = row.markers)
        city_marker[row.cities] = row.markers

    return city_marker

def plot_and_save(df, colors, scenarios, x = "", y = ""):
    city_marker = plot_x_y(df, x, y)
    save_fig(colors, scenarios, city_marker, False, x, y)

def plot_ci_over_cn(cfg, scale_x = False):

    cities = cfr.get_cities(cfg)
    colors = cfr.get_colors(cfg)
    scenarios = cfr.get_scenario_names(cfg)
    markers = cfr.get_markers(cfg)

    replaces_color = dict(zip(scenarios, colors))
    replaces_markers = dict(zip(cities, markers))

    df = pd.DataFrame()
    for city in cities:
        df = pd.concat([df, pd.read_csv(Path("out", "{}_components.csv".format(city)))])
    
    df.columns = ("counter", "cities", "scenario", "biggest_component_coverage", "component_count")

    #print(df)
    df[["color"]] = df[["scenario"]].replace(replaces_color)
    df[["marker"]] = df[["cities"]].replace(replaces_markers)

    df3 = pd.read_csv(Path("out", "node_count.csv"))
    print(df3)
    node_counts = []
    for row in df.itertuples():
        # print(row)
        for row3 in df3.itertuples():
            if row.cities == row3.cities:
                node_counts.append(row3.node_count)

    #df = df.replace(replaces_markers)
    df["node_count"] = node_counts
    print(df)
    df["component_count_norm"] = df["component_count"] / df["node_count"]
    #df = df.replace(replaces_markers)
    #print(df)
    # can#t set markers as a list so iterate instead
    # ax = df.plot.scatter(x = "2", y = "3", c = "1", marker = markers)

    #df = df.rename(columns={"0": "counter",  1: "marker", 2:"color", 3:"biggest_percentage_component", 4:"component_count"})
    print(df)
    #df3 = get_cities_results_as_df(cfg, scale_x = scale_x)
    df2 = get_cities_results_as_df(cfg, scale_x = scale_x)
    print(df2)
    df2["biggest_component_coverage"] = df["biggest_component_coverage"].values
    df2["component_count"] = df["component_count"].values
    df2["component_count_norm"] = df["component_count_norm"].values
    df2["node_count"] = df["node_count"].values
    

    possibles = ["biggest_component_coverage", "component_count", "detours", "unreachables", "component_count_norm", "node_count"]

    for x in possibles:
        for y in possibles:
            plot_and_save(df2, colors, scenarios, x, y)
            plt.clf()

if __name__ == '__main__':
    
    #plt.rcParams['text.usetex'] = True

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--config",
        help="Name or absolute Path of a .ini file to use.",
        default="ready4robots.ini")
    
    args = parser.parse_args()

    cfg = ConfigObj(args.config)
    
    ox.config(useful_tags_way=cfr.get_relevant_tags(cfg))

    cities = cfr.get_cities(cfg)
    

    for city in cities:
        city_plots(cfg, city, override = False)
        # folium_test(city, cfg)
        
    # cities_plots(cfg, scale_x = False, override=True)
    # cities_plots(cfg, False, override=True)

    # plot_components(cfg, normalize = False)
    # plot_components(cfg, zoomed = True, normalize = False)
    # plot_components(cfg, normalize = True)
    # plot_components(cfg, zoomed = True, normalize = True)

    # plot_ci_over_cn(cfg, scale_x = False)