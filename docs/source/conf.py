# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# https://www.sphinx-doc.org/en/master/usage/configuration.html

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
import sys
from pathlib import Path

# go up two levels from /docs/source to the package root
sys.path.insert(0, str(Path().resolve().parent.parent))

# mock import these packages because readthedocs doesn't have them installed
autodoc_mock_imports = [
    "configobj",
    "dateutil",
    "folium",
    "geopandas",
    "igraph",
    "matplotlib",
    "matplotlib.cm",
    "matplotlib.colors",
    "matplotlib.pyplot",
    "networkx",
    "numpy",
    "osgeo",
    "osmnx",
    "pandas",
    "psutil",
    "pyproj",
    "rasterio",
    "requests",
    "rtree",
    "scipy",
    "scipy.spatial",
    "shapely",
    "shapely.geometry",
    "shapely.ops",
    "sklearn",
    "sklearn.neighbors",
    "tqdm",
]

# -- Project information -----------------------------------------------------

project = 'ReadyForRobots'
copyright = '2021, Plank'
author = 'Plank'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    'sphinx.ext.autodoc',
    # 'numpydoc',
    'sphinx.ext.intersphinx',
    'sphinx.ext.napoleon'
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []


# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'default'

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']
