.. ReadyForRobots documentation master file, created by
   sphinx-quickstart on Tue Dec 14 18:18:32 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to ReadyForRobots's documentation!
==========================================

Where autonomous vehicles (AV) are allowed to drive is not well defined yet.
Different vehicles may prefer different types of streets as well.
Which streets a particular AV is allowed to drive on and which other restrictions may apply will be called a ruleset.

If you use this code in your work, please cite the journal article:

Added Once the review is done.

Installation
------------

You can either use `docker` or `conda`_:

Docker:
^^^^^^^

Clone the Repository on your local machine, navigate to the docker folder and build the image.
Then simply start a container in interactive mode and use the code.
It's recommended that you mount at least one volume to export the results.

.. code-block:: shell

   docker build -t image-name .
   docker run -it
        -v mountOnDisk:mountInContainer:options
        --name nameOfContainer image-name

.. _conda: https://conda.io/
.. _docker: https://www.docker.com

Anaconda:
^^^^^^^^^

If you are not able or don't want to use docker you can  install the packages with a package manager of your choice. However [[OSMnx]][osmnx] does not work with pip out of the box as mentioned by the author in this [Issue 180](https://github.com/gboeing/osmnx/issues/180#issuecomment-404046637). So it is recommended to use Anaconda.

You can create the environment from our environment.yml:

.. code-block:: shell
   
   conda env create -f environment.yml

or manually:

.. code-block:: shell

   conda install -c 
      conda-forge 
      osmnx 
      python-igraph 
      configobj 
      tqdm 
      psutil
      folium

Some version of packages may not be working together.
This is a working configuration:

.. code-block:: shell

   conda install -c conda-forge osmnx=1.0.0 configobj=5.0.6 python-igraph=0.8.3 tqdm=4.59.0 psutil=5.8.0 ipython=7.18.1

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ready4robots

Usage
-----

You can see some Ideas in the Notebooks in the `supplements` folder or in this [Binder]().


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
