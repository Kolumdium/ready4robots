# -*- coding: utf-8 -*-
"""
Created on Mon Mar  8 15:27:52 2021

@author: plank
"""

import pandas as pd
from pathlib import Path
from matplotlib.lines import Line2D
from matplotlib import rcParams, cycler
import matplotlib as mpl
import matplotlib.pyplot as plt
import numpy as np
import math

import osmnx as ox
import argparse
from configobj import ConfigObj
import configreader as cfr
import ready4robots as osmq
import osmhelper as oh
import networkx as nx
import matplotlib.lines as mlines
import matplotlib.ticker as mtick

import configreader as cfr
from mpl_toolkits.axes_grid1 import make_axes_locatable

def make_custom_legend(colors, names, short_names = [], markers = None):

    custom_lines = []
    if markers is None:
        markers = [1 for x in range(0, len(names))]
    for color, name, short_name, marker in zip(colors, names, short_names, markers):
        label = f"{short_name} ({name})"
        custom_lines.append(Line2D([0], [0], color=color, lw=4, label=label, marker = marker))

    return custom_lines

def encode_marker(value):
    x = value
    try:
        if len(value) > 1:
            print(value)
            x = "$\{0}$".format(value)
    except Exception as e:
        print(e)
    return x

def save_fig(colors, scenarios, city_marker = [], short_names = [],
             sci = False, x = "", y = "",
             title = False, filename = None, line = False,
             legend = True, xlim = None, ylim = None):
    ax = plt.gca()
    ax.set_axisbelow(True)
    ax.grid(True, "major", "both")
    ax.tick_params(labelsize = 14)

    custom_lines = make_custom_legend(colors, scenarios, short_names)

    custom_markers = []
    for key, value in city_marker.items():
        encoded_marker = encode_marker(value)
        custom_markers.append(Line2D([0], [0], color = "black", linestyle='', markerfacecolor='black', markersize=10, label = key, marker = encoded_marker))

    custom_lines.extend(custom_markers)

    if legend == "top":
        ax.legend(handles=custom_lines, bbox_to_anchor=(0., 1.02, 1., .102), loc='lower left',
            ncol=3, mode="expand", borderaxespad=0.)
    if legend == "right":
        ax.legend(handles=custom_lines, bbox_to_anchor=(1,1), loc="upper left")  

    ax.set_xlabel("{}".format(x), fontsize = 15)
    ax.set_ylabel("{}".format(y), fontsize = 15)
    if title is None:
        ax.set_title("{} over {}".format(y, x), fontsize = 15)
    elif title:
        ax.set_title(title, fontsize = 15)
    if sci:
        ax.ticklabel_format(style='sci', axis='y')
        ax.yaxis.set_major_formatter(mtick.FormatStrFormatter('%.0e'))
        formatter = mtick.ScalarFormatter(useMathText=True)
        formatter.set_scientific(True) 
        formatter.set_powerlimits((-1,1)) 
        ax.yaxis.set_major_formatter(formatter) 

    if line:
        x1, y1 = [0], [0]
        x2, y2 = [1], [1]
        plt.plot((-1,2), (-1,2), marker = 'o')
    
    if not xlim is None:
        plt.xlim(xlim)
    if not ylim is None:
        plt.ylim(ylim)
        # plt.ylim(0, 1)
    fig = ax.get_figure()
    
    if filename:
        filepath = Path("out", filename)
    else:
        filepath = Path("out", "{}_over_{}.png".format(y,x))

    fig.savefig(filepath, bbox_inches='tight')

def plot_x_y(df, param1, param2, emphasis = "Freiberg, Sachsen"):
    city_marker = {}

    df["size"] = 40 

    # print(df.columns)

    if not emphasis is None:
        df["size"].iloc[df["city"] == emphasis] = 200

    for row in df.itertuples():
        #handle unicode markers
        x = encode_marker(row.marker)
        plt.scatter(x = getattr(row, param1), y = getattr(row, param2),
                    c = row.color, s = row.size, alpha = 0.9,
                    marker = x)
        city_marker[row.city] = row.marker

    return city_marker

def plot_and_save(df, colors, scenarios, x = "", y = "", emphasis = None):
    city_marker = plot_x_y(df, x, y, emphasis = "Freiberg, Sachsen")
    save_fig(colors, scenarios, city_marker, False, x, y)


def plot_x_y_save(df, colors, scenarios, short_names,
                x, y, xl = None, yl = None,
                filename = None, extension = "pdf",
                title = False, legend = False, line = False,
                xlim = None, ylim = None, emphasis = None):
    
    #turn on latex notation 
    mpl.rcParams['text.usetex'] = True

    if xl is None:
        xl = r"${}$".format(x).replace("_", "\_")
        # mpl.rcParams['text.usetex'] = False

    if yl is None:
        yl = r"${}$".format(y).replace("_", "\_")
        # mpl.rcParams['text.usetex'] = False

    markers = plot_x_y(df, x, y, emphasis = emphasis)
    if filename is None:
        filename = "{}_{}.{}".format(x,y,extension)
    save_fig(colors = colors, scenarios = scenarios, city_marker = markers,
            short_names = short_names,
            sci=False, x=xl, y = yl,
            title = title, filename = filename,
            legend = legend, line = line, xlim = xlim,
            ylim = ylim)
    mpl.rcParams['text.usetex'] = False
    plt.clf()

def plot_important(df, cfg):
    cities = cfr.get_cities(cfg)
    colors = cfr.get_colors(cfg)
    scenarios = cfr.get_scenario_names(cfg)
    markers = cfr.get_markers(cfg)
    short_names = cfr.get_short_names(cfg)

    # print(df)
    # return
    # markers[0] = "$\u25b2$"

    plot_x_y_save(df, colors, scenarios, short_names,
        "max_nodes_scenario_percentage", "detour", r"$f_i$", r"$\Delta T(G_i)$",
        legend = "top", xlim = (-0.05, 1.05), ylim = (-0.2,7), emphasis = "Freiberg, Sachsen")
    plot_x_y_save(df, colors, scenarios, short_names,
        "component_count_norm", "detour", r"$c(G_i)$" ,r"$\Delta T(G_i)$", 
        legend = "top", xlim = (-0.05, 1.05), ylim = (-0.2,7))
    plot_x_y_save(df, colors, scenarios, short_names,
        "biggest_component_coverage", "detour", r"$k(c_{max})$", r"$\Delta T(G_i)$", 
        legend = "top", xlim = (-0.05, 1.05), ylim = (-0.2,7), emphasis = "Freiberg, Sachsen")
    plot_x_y_save(df, colors, scenarios, short_names,
        "biggest_component_coverage", "component_count_norm", r"$k(c_{max})$", r"$c(G_i)$", 
        legend = "top", xlim = (-0.05, 1.05), ylim = (-0.05,1.05))
    plot_x_y_save(df, colors, scenarios, short_names,
        "component_count_norm", "biggest_component_coverage", r"$c(G_i)$", r"$k(c_{max})$", 
        legend = "top", xlim = (-0.05, 1.05), ylim = (-0.05,1.05))
    plot_x_y_save(df, colors, scenarios, short_names,
        "biggest_component_coverage", "average_nodes_reachable_percentage_all", r"$k(c_{max})$", r"$\mathcal{R}_{avg}(G_i)$", 
        line = True, legend = "top", xlim = (-0.05, 1.05), ylim = (-0.05,1.05))
    plot_x_y_save(df, colors, scenarios, short_names,
        "component_count_norm", "average_nodes_reachable_percentage_all", r"$c(G_i)$", r"$\mathcal{R}_{avg}(G_i)$", 
        legend = "top", xlim = (-0.05, 1.05), ylim = (-0.05,1.05))
    plot_x_y_save(df, colors, scenarios, short_names,
        "max_nodes_scenario_percentage", "average_nodes_reachable_percentage_all",r"$f_i$",r"$\mathcal{R}_{avg}(G_i)$", 
        legend = "top", xlim = (-0.05, 1.05), ylim = (-0.05,1.05))
    plot_x_y_save(df, colors, scenarios, short_names,
        "average_nodes_reachable_percentage_all", "detour", r"$\mathcal{R}_{avg}(G_i)$", r"$\Delta T(G_i)$", 
        legend = "top", xlim = (-0.05, 1.05), ylim = (-0.2,7))
    plot_x_y_save(df, colors, scenarios, short_names,
        "h1800n", "detour", None, r"$\Delta T(G_i)$", 
        legend = "top", xlim = (-0.05, 1.05), ylim = (-0.2,7))
    plot_x_y_save(df, colors, scenarios, short_names,
        "max_ways", "average_nodes_reachable_percentage_all", None, r"$\mathcal{R}_{avg}(G_i)$", 
        legend = "top", xlim = (-0.05, 1.05), ylim = (-0.05, 1.05))

def main(cfg, override):
    ox.config(useful_tags_way=cfr.get_relevant_tags(cfg))
    ox.config(use_cache=True, log_console=True)
    key_numbers = osmq.calc_key_numbers(cfg, override=override)

    df = pd.DataFrame(key_numbers)
    plot_important(df, cfg)

if __name__ == '__main__':
    
    #plt.rcParams['text.usetex'] = True

    parser = argparse.ArgumentParser()

    parser.add_argument(
        "--config",
        help="Name or absolute Path of a .ini file to use.",
        default="ready4robots.ini")
    
    args = parser.parse_args()

    cfg = ConfigObj(args.config)
    
    ox.config(useful_tags_way=cfr.get_relevant_tags(cfg))

    cities = cfr.get_cities(cfg)
    
    main(cfg, override = False)