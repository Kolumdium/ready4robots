
# ready4robots

ready4robots is a python package going along with the publication _.


## Goals

Where autonomous vehicles (AV) are allowed to drive is not well defined yet.
On the one hand because the vehicles are very heterogeneous in their field of work and thus in their appearance, on the other hand are cities structured very differently.
Additionally the acceptance of the local populous may vary as well.

Which streets a particular AV is allowed to drive on and which other restrictions may apply is defined in a set of rules.

This Package provides different ways of evaluating these rule sets with regard to accessability.

## References

For the underlying analytics of [Openstreetmap](https://www.openstreetmap.org/#map=12/50.9173/13.3333) data we heavily rely on the [OSMnx package](https://github.com/gboeing/osmnx) as well as [igraph for python](https://igraph.org/python/).

Boeing, G. 2017. `OSMnx: New Methods for Acquiring, Constructing, Analyzing, and Visualizing Complex Street Networks`_. *Computers, Environment and Urban Systems* 65, 126-139. doi:10.1016/j.compenvurbsys.2017.05.004

## Installation

   Please refer to the [Installation instructions](https://ready4robots.readthedocs.io/en/latest/index.html) for details.

## Usage and Examples

You can see some Ideas in the Notebooks in the `supplements` folder or in this [Binder]().

## More

For more Information have a look at the [Documentation](https://ready4robots.readthedocs.io/en/latest/index.html).