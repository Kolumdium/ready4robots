import os

from setuptools import setup

LONG_DESCRIPTION = r"""This is where the long description should go."""

DESC = "short and sweat"
# list of classifiers from the PyPI classifiers trove
CLASSIFIERS = []

# only specify install_requires if not in RTD environment
if os.getenv("READTHEDOCS") == "True":
    INSTALL_REQUIRES = []
else:
    with open("requirements.txt") as f:
        INSTALL_REQUIRES = [line.strip() for line in f.readlines()]


# now call setup
setup(
    name="ready4robots",
    version="0.1.0",
    description=DESC,
    long_description=LONG_DESCRIPTION,
    classifiers=CLASSIFIERS,
    url="https://gitlab.com/Kolumdium/ready4robots",
    author="Martin Plank",
    author_email="martin.plank@informatik.tu-freiberg.de",
    license="MIT",
    platforms="any",
    packages=[],
    python_requires=">=3.7",
    install_requires=INSTALL_REQUIRES,
    extras_require={},
)